package org.kshrd.views;

import org.kshrd.models.dto.Student;

import java.util.List;

/**
 * Created by temchannat on 5/8/18.
 */
public class StudentView {

    public void printStudentInformation(List<Student> students) {
        students.forEach(student -> {
            System.out.println(student.getId()
                    + "\t" + student.getName()
                    + "\t" + student.getGender()
            );
        });

    }
}
