package org.kshrd;

import org.kshrd.controllers.StudentController;
import org.kshrd.models.database.ConnectToDatabase;
import org.kshrd.models.dto.Student;

public class Main {

    public static void main(String[] args) {

        StudentController studentController = new StudentController();

        studentController.viewStudentInformation();


        Student student = new Student("Reach", "M");
        studentController.addStudent(student);

        studentController.viewStudentInformation();


    }
}
