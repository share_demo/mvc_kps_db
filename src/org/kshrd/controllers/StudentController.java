package org.kshrd.controllers;

import org.kshrd.models.dao.StudentData;
import org.kshrd.models.dto.Student;
import org.kshrd.views.StudentView;

import java.util.List;

/**
 * Created by temchannat on 5/8/18.
 */
public class StudentController {

    List<Student> students;
    StudentView studentView = new StudentView();
    StudentData studentData = new StudentData();

    public void viewStudentInformation() {
        students = studentData.studentList();
        studentView.printStudentInformation(students);
    }

    public void addStudent(Student student) {
        studentData.addStudent(student);
    }

}
