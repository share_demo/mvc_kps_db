package org.kshrd.models.dao;

import org.kshrd.models.database.ConnectToDatabase;
import org.kshrd.models.dto.Student;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by temchannat on 5/8/18.
 */
public class StudentData {

    ConnectToDatabase connectToDatabase = new ConnectToDatabase();

    public List<Student> studentList() {
        List<Student> students = new ArrayList<>();
        String sql = "SELECT * FROM tbl_students";
        try {
            connectToDatabase.openConnection();
            Statement statement = connectToDatabase.connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while(resultSet.next()) {
                Student student = new Student(resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3));
                students.add(student);
            }

            connectToDatabase.closeConnection();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return students;
    }


    public void addStudent(Student student) {
        connectToDatabase.openConnection();
        String sql = "INSERT INTO tbl_students (name, gender) " +
                "VALUES (?, ?)";
        try {
            PreparedStatement preparedStatement = connectToDatabase.connection.prepareStatement(sql);
            preparedStatement.setString(1, student.getName());
            preparedStatement.setString(2, student.getGender());
            int isSuccess = preparedStatement.executeUpdate();
            if(isSuccess>0) {
                System.out.println("Insert success.....");

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        connectToDatabase.closeConnection();
    }


}
